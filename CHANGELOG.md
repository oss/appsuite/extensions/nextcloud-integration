# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.3.2] - 2025-01-16
- Prepare for base image update

## [1.3.1] - 2024-11-29
- NCLD-54: Filepicker for meeting attachments does not work

## [1.3.0] - 2024-09-27
- ES-974: Apply consistent spelling of "email"

## [1.2.0] - 2023-12-11
- NCLD-45: Missing error message if password is too "easy"

## [1.1.0] - 2023-10-31
- Updates for 8.19

## [1.0.5] - 2023-09-25
- NCLD-49: Integrate PDF Export with Nextcloud

## [1.0.4] - 2023-09-25
- NCLD-49: Integrate PDF Export with Nextcloud

## [1.0.3] - 2023-08-30
- NCLD-44: Missing translations to English

## [1.0.2] - 2023-08-03
- NCLD-48: File Picker missing in mail compose

## [1.0.1] - 2023-05-17
- NCLD-43: Dropdown in mail compose broken after autosave
- PBSR-637: Integrate FilePicker in OX8 for modules calendar, contacts, tasks

## [1.0.0] - 2023-05-09
- NCLD-41: Upgrade UI customization to OX 8

## [0.0.1]

[unreleased]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.3.2...main
[1.3.2]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/middleware-public-sector-pro-8.33.56...1.3.2
[1.3.1]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/middleware-public-sector-pro-8.31.44...1.3.1
[1.3.0]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/middleware-public-sector-8.25.46...1.3.0
[1.2.0]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.0.5...1.1.0
[1.0.5]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.0.4...1.0.5
[1.0.4]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.0.3...1.0.4
[1.0.3]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.open-xchange.com/extensions/nextcloud-integration/compare/middleware-public-sector-8.12.2...1.0.0
