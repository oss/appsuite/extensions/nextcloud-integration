/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.nextcloud.filepicker.impl;

import static com.openexchange.file.storage.nextcloud.oauth.N.logger;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.json.JSONException;
import org.json.JSONInputStream;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.annotation.Nullable;
import com.openexchange.config.lean.DefaultProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.java.Streams;
import com.openexchange.java.Strings;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.rest.client.httpclient.HttpClients;
import com.openexchange.rest.client.httpclient.ManagedHttpClient;
import com.openexchange.session.Session;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;
import com.openexchange.webdav.client.BearerAuthScheme;

@NonNullByDefault
public class NextcloudShareLinkAction extends SafeAJAXActionService {

    private static final Logger LOG = logger(NextcloudShareLinkAction.class);

    private static final String CHARSET = "UTF-8";

    private final HttpClientService        httpClientService;
    private final LeanConfigurationService leanConf;

    public NextcloudShareLinkAction(HttpClientService httpClientService, LeanConfigurationService leanConf) {
        super();
        this.httpClientService = httpClientService;
        this.leanConf = leanConf;
    }

    @Override
    public AJAXRequestResult performNullChecked(AJAXRequestData requestData, ServerSession session) throws OXException {

        // requestData must contain a json
        Object data = requestData.getData();
        if (null == data) {
            throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create();
        }
        if (false == JSONObject.class.isInstance(data)) {
            throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create();
        }
        JSONObject dataObject = (JSONObject) data;

        JSONObject dataFromNextcloud = executeOnNextcloud(session, () -> {
            HttpPost post = new HttpPost(getHost(session));
            setPostBody(dataObject, post);
            return post;
        });
        if (dataObject.hasAndNotNull("permissions") && updateLinkPermissions(session)) {
            final String shareId = dataFromNextcloud.optString("id");
            int permissions = dataObject.optInt("permissions", -1);
            if (null != shareId && permissions > 0) {
                executeOnNextcloud(session, () -> {
                    HttpPut put = new HttpPut(getHost(session, shareId));
                    setPutBody(permissions, put);
                    return put;
                });
            }
        }
        String url = getFromJson(dataFromNextcloud, "url");
        return new AJAXRequestResult(new JSONObject().putSafe("url", url));
    }

    protected JSONObject executeOnNextcloud(ServerSession session, ExceptionThrowingVoidFunction<HttpEntityEnclosingRequestBase> baseProvider) throws OXException {
        ManagedHttpClient httpClient = getClient();
        HttpEntityEnclosingRequestBase requestBase = null;
        HttpResponse response = null;
        try {
            requestBase = baseProvider.accept();
            requestBase.setHeader("OCS-APIRequest", "true");
            requestBase.setHeader("Accept", "application/json");
            setupBearerAuth(requestBase, getAccessTokenFromSession(session));
            response = httpClient.execute(requestBase);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 100 || statusCode == 200) {
                // get shareId from body
                try (InputStream inputStream = Streams.bufferedInputStreamFor(response.getEntity().getContent())) {
                    return getDataFromInputstream(inputStream);
                }
            }
            if (statusCode == 401) {
                try (InputStream inputStream = Streams.bufferedInputStreamFor(response.getEntity().getContent())) {
                    String error = getErrorFromInputStream(inputStream);
                    if (!Strings.isEmpty(error)) {
                        throw NextcloudExceptionCodes.AUTH_FAILED.create(error);
                    }
                }
                throw NextcloudExceptionCodes.AUTH_FAILED.create("401 returned from Nextcloud");
            }
            if (statusCode == 403) {
                // try to get the error
                try (InputStream inputStream = Streams.bufferedInputStreamFor(response.getEntity().getContent())) {
                    String error = getErrorFromInputStream(inputStream);
                    if (!Strings.isEmpty(error)) {
                        throw NextcloudExceptionCodes.NO_LINK_USER_INPUT.create(error);
                    }
                }
                throw NextcloudExceptionCodes.NO_LINK_USER_INPUT.create("Forbidden by Nextcloud");
            }
            throw NextcloudExceptionCodes.NO_LINK.create(String.format("Status code returned '%s'", statusCode));
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            LOG.debug("unable to create share for path {}, path", e);
            throw NextcloudExceptionCodes.UNEXPECTED_ERROR.create(e.getMessage(), e);
        } finally {
            HttpClients.close(requestBase, response);
        }
    }

    protected void setPostBody(JSONObject dataObject, HttpEntityEnclosingRequestBase requestBase) throws OXException {
        JSONObject json = new JSONObject();
        {
            // path is mandatory
            final String path;
            try {
                path = dataObject.getString("path");
            } catch (JSONException e) {
                throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create(e.getMessage(), e);
            }
            if (null == path || Strings.isEmpty(path)) {
                throw AjaxExceptionCodes.MISSING_REQUEST_BODY.create("path is empty");
            }
            json.putSafe("path", path);
        }
        setOptionalParameter(dataObject, json, "shareType", 3);
        setOptionalParameter(dataObject, json, "shareWith", null);
        setOptionalParameter(dataObject, json, "publicUpload", null);
        setOptionalParameter(dataObject, json, "password", null);
        setOptionalParameter(dataObject, json, "permissions", null);
        setOptionalParameter(dataObject, json, "expireDate", null);
        LOG.debug("creating link with json {}", json);
        InputStreamEntity entity = new InputStreamEntity(new JSONInputStream(json, com.openexchange.java.Charsets.UTF_8_NAME), -1L, ContentType.APPLICATION_JSON);
        requestBase.setEntity(entity);
    }

    protected void setPutBody(int permissions, HttpEntityEnclosingRequestBase requestBase) throws OXException {
        JSONObject json = new JSONObject();
        json.putSafe("permissions", permissions);
        InputStreamEntity entity = new InputStreamEntity(new JSONInputStream(json, com.openexchange.java.Charsets.UTF_8_NAME), -1L, ContentType.APPLICATION_JSON);
        requestBase.setEntity(entity);
    }

    protected void setOptionalParameter(JSONObject dataObject, JSONObject body, String key, @Nullable Object optDefault) {
        final Object value = dataObject.opt(key);
        if (null != value) {
            body.putSafe(key, value);
            return;
        }
        if (null != optDefault) {
            body.putSafe(key, optDefault);
            return;
        }
    }

    protected JSONObject getDataFromInputstream(@Nullable InputStream inputStream) throws IOException, JSONException, OXException {
        String string = Streams.stream2string(inputStream, CHARSET);
        // assume json
        JSONObject jsonObject = new JSONObject(string);
        LOG.debug("response json: {}", jsonObject);
        JSONObject ocs = jsonObject.optJSONObject("ocs");
        if (null == ocs) {
            throw NextcloudExceptionCodes.NO_LINK.create("ocs json missing");
        }
        JSONObject data = ocs.optJSONObject("data");
        if (null == data) {
            throw NextcloudExceptionCodes.NO_LINK.create("ocs.data json missing");
        }
        return data;
    }

    protected String getFromJson(JSONObject data, String identifier) throws OXException {
        String url = data.optString(identifier);
        if (url == null) {
            throw NextcloudExceptionCodes.NO_LINK.create(identifier + " json missing");
        }
        return url;
    }

    protected String getErrorFromInputStream(@Nullable InputStream inputStream) throws IOException, JSONException, OXException {
        String string = Streams.stream2string(inputStream, CHARSET);
        // assume json
        JSONObject jsonObject = new JSONObject(string);
        JSONObject ocs = jsonObject.optJSONObject("ocs");
        if (null == ocs) {
            return "";
        }
        JSONObject meta = ocs.optJSONObject("meta");
        if (null == meta) {
            return "";
        }
        String message = meta.optString("message");
        if (message == null) {
            return "";
        }
        return message;
    }

    protected String getHost(ServerSession session) throws OXException {
        String base = leanConf.getProperty(session.getUserId(), session.getContextId(), DefaultProperty.valueOf("com.openexchange.file.storage.nextcloud.oauth.url", ""));
        if (null == base || Strings.isEmpty(base)) {
            throw NextcloudExceptionCodes.HOST_NOT_CONFIGURED.create();
        }
        if (!base.endsWith("/")) {
            base += "/";
        }
        return base + "ocs/v2.php/apps/files_sharing/api/v1/shares";
    }

    protected String getHost(ServerSession session, String shareId) throws OXException {
        return getHost(session) + "/" + shareId;
    }

    protected boolean updateLinkPermissions(ServerSession session) throws OXException {
        return leanConf.getBooleanProperty(session.getUserId(), session.getContextId(), DefaultProperty.valueOf("com.openexchange.nextcloud.filepicker.updatePermissions", Boolean.TRUE));
    }

    protected String getAccessTokenFromSession(ServerSession session) throws OXException {
        Object parameter = session.getParameter(Session.PARAM_OAUTH_ACCESS_TOKEN);
        if (null == parameter) {
            throw NextcloudExceptionCodes.TOKEN_MISSING.create("no token present in session");
        }
        if (parameter instanceof String) {
            return (String) parameter;
        }
        throw NextcloudExceptionCodes.TOKEN_MISSING.create("token present but in wrong type in session");
    }

    protected ManagedHttpClient getClient() {
        return httpClientService.getHttpClient(NextcloudHttpCLientConfigProvider.CLIENTID);
    }

    private void setupBearerAuth(HttpEntityEnclosingRequestBase requestBase, String token) throws OXException {
        try {
            Header authenticate = new BearerAuthScheme().authenticate(new UsernamePasswordCredentials(token, null), requestBase);
            requestBase.addHeader(authenticate);
        } catch (AuthenticationException e) {
            throw NextcloudExceptionCodes.AUTH_FAILED.create(e.getMessage(), e);
        }
    }

}
