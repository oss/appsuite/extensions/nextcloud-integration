package com.openexchange.nextcloud.filepicker.impl;

import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.exception.OXException;

@FunctionalInterface
@NonNullByDefault
public interface ExceptionThrowingVoidFunction<R> {
    R accept() throws OXException;
}