/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth.osgi;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import com.openexchange.annotation.NonNullByDefault;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageAccountManager;
import com.openexchange.file.storage.FileStorageAccountManagerProvider;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.osgi.RankingAwareNearRegistryServiceTracker;
import com.openexchange.session.Session;

/**
 * {@link NextcloudOauthFileStorageAccountManagerTracker}
 * 
 * @author <a href="mailto:felix.marx@open-xchange.com">Felix Marx</a>
 * @since v7.10.5
 */
@NonNullByDefault
public class NextcloudOauthFileStorageAccountManagerTracker extends RankingAwareNearRegistryServiceTracker<FileStorageAccountManagerProvider> {

    private BundleContext bundleContext;

    /**
     * Dummy value.
     */
    protected static final Object PRESENT = new Object();

    /**
     * Initializes a new {@link NextcloudOauthFileStorageAccountManagerTracker}.
     *
     * @param bundleContext The OSGi bundle execution context
     */
    NextcloudOauthFileStorageAccountManagerTracker(BundleContext bundleContext) {
        super(bundleContext, FileStorageAccountManagerProvider.class, 0);
        this.bundleContext = bundleContext;
        this.providers = new ConcurrentHashMap<FileStorageAccountManagerProvider, Object>(8, 0.9f, 1);
    }

    /**
     * The backing queue.
     */
    protected final ConcurrentMap<FileStorageAccountManagerProvider, Object> providers;

    private static final String PARAM_DEFAULT_ACCOUNT = "file.storage.defaultAccount";

    public FileStorageAccountManager getAccountManager(final String accountId, final Session session, FileStorageAccountManagerProvider ignore) throws OXException {
        final String paramName = new StringBuilder(PARAM_DEFAULT_ACCOUNT).append('@').append(accountId).toString();
        FileStorageAccountManager accountManager = (FileStorageAccountManager) session.getParameter(paramName);
        if (null == accountManager) {
            FileStorageAccountManagerProvider candidate = null;
            for (final FileStorageAccountManagerProvider provider : providers.keySet()) {
                if (ignore == provider) {
                    continue;
                }
                if ((null == candidate) || (provider.getRanking() > candidate.getRanking())) {
                    final FileStorageAccountManager cAccountManager = provider.getAccountManager(accountId, session);
                    if (null != cAccountManager) {
                        candidate = provider;
                        accountManager = cAccountManager;
                    }
                }
            }
            if (null == accountManager) {
                return null;
            }
            session.setParameter(paramName, accountManager);
        }
        return accountManager;
    }

    public FileStorageAccountManager getAccountManagerFor(final String serviceId, FileStorageAccountManagerProvider ignore) throws OXException {
        if (null == serviceId) {
            final String msg = "serviceId is null";
            throw FileStorageExceptionCodes.UNEXPECTED_ERROR.create(new NullPointerException(msg), msg);
        }

        FileStorageAccountManagerProvider candidate = null;
        for (final FileStorageAccountManagerProvider provider : providers.keySet()) {
            if (ignore == provider) {
                continue;
            }
            if (provider.supports(serviceId) && ((null == candidate) || (provider.getRanking() > candidate.getRanking()))) {
                candidate = provider;
            }
        }
        if (null == candidate) {
            throw FileStorageExceptionCodes.NO_ACCOUNT_MANAGER_FOR_SERVICE.create(serviceId);
        }
        return candidate.getAccountManagerFor(serviceId);
    }

    @Override
    public FileStorageAccountManagerProvider addingService(final ServiceReference<FileStorageAccountManagerProvider> reference) {
        final BundleContext context = bundleContext;
        final FileStorageAccountManagerProvider service = context.getService(reference);
        {
            if (null == providers.putIfAbsent(service, PRESENT)) {
                return service;
            }
        }
        /*
         * Adding to registry failed
         */
        context.ungetService(reference);
        return null;
    }

    @Override
    public void modifiedService(final ServiceReference<FileStorageAccountManagerProvider> reference, final FileStorageAccountManagerProvider service) {
        // Nothing to do
    }

    @Override
    public void removedService(final ServiceReference<FileStorageAccountManagerProvider> reference, final FileStorageAccountManagerProvider service) {
        if (null != service) {
            try {
                providers.remove(service);
            } finally {
                bundleContext.ungetService(reference);
            }
        }
    }
} // End of Customizer class
