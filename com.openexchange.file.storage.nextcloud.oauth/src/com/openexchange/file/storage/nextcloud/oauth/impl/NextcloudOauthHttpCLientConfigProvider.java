/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth.impl;

import com.openexchange.config.lean.DefaultProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.rest.client.httpclient.DefaultHttpClientConfigProvider;
import com.openexchange.rest.client.httpclient.HttpBasicConfig;


/**
 * {@link NextcloudHttpCLientConfigProvider}
 *
 * @author <a href="mailto:felix.marx@open-xchange.com">Felix Marx</a>
 */
public class NextcloudOauthHttpCLientConfigProvider extends DefaultHttpClientConfigProvider {
    private static final String MAX_CONNECTIONS            = "com.openexchange.file.storage.nextcloud.oauth.maxConnections";
    private static final String MAX_CONNECTIONS_PER_HOST   = "com.openexchange.file.storage.nextcloud.oauth.maxConnectionsPerHost";
    private static final String CONNECTION_TIMEOUT         = "com.openexchange.file.storage.nextcloud.oauth.connectionTimeout";
    private static final String SOCKET_READ_TIMEOUT        = "com.openexchange.file.storage.nextcloud.oauth.socketReadTimeout";
    private static final String CONNECTION_REQUEST_TIMEOUT = "com.openexchange.file.storage.nextcloud.oauth.connectionRequestTimeout";

    public static final String CLIENTID = "nextcloud-oauth";

    private final LeanConfigurationService leanConfigurationService;

    /**
     * Initializes a new {@link NextcloudHttpCLientConfigProvider}.
     */
    public NextcloudOauthHttpCLientConfigProvider(LeanConfigurationService leanConfigurationService) {
        super(CLIENTID, "Open-Xchange Nextcloud Client");
        this.leanConfigurationService = leanConfigurationService;
    }

    @Override
    public HttpBasicConfig configureHttpBasicConfig(HttpBasicConfig config) {
        int maxConnections = leanConfigurationService.getIntProperty(DefaultProperty.valueOf(MAX_CONNECTIONS, Integer.valueOf(100)));
        config.setMaxTotalConnections(maxConnections);

        int maxConnectionsPerHost = leanConfigurationService.getIntProperty(DefaultProperty.valueOf(MAX_CONNECTIONS_PER_HOST, Integer.valueOf(100)));
        config.setMaxConnectionsPerRoute(maxConnectionsPerHost);

        int connectionTimeout = leanConfigurationService.getIntProperty(DefaultProperty.valueOf(CONNECTION_TIMEOUT, Integer.valueOf(3000)));
        config.setConnectTimeout(connectionTimeout);

        int socketReadTimeout = leanConfigurationService.getIntProperty(DefaultProperty.valueOf(SOCKET_READ_TIMEOUT, Integer.valueOf(6000)));
        config.setSocketReadTimeout(socketReadTimeout);

        int connectionRequestTimeout = leanConfigurationService.getIntProperty(DefaultProperty.valueOf(CONNECTION_REQUEST_TIMEOUT, Integer.valueOf(3000)));
        config.setConnectionRequestTimeout(connectionRequestTimeout);

        return config;
    }

}
