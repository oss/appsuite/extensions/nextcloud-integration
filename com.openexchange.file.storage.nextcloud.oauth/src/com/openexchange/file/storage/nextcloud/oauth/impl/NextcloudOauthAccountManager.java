/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.lean.DefaultProperty;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageAccount;
import com.openexchange.file.storage.FileStorageAccountManager;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.file.storage.FileStorageService;
import com.openexchange.file.storage.ServiceAware;
import com.openexchange.file.storage.nextcloud.oauth.NextcloudLoginAndWebdavUrlProvider;
import com.openexchange.file.storage.registry.FileStorageServiceRegistry;
import com.openexchange.session.Session;

/**
 * {@link NextcloudOauthAccountManager}
 *
 * @author <a href="mailto:felix.marx@open-xchange.com">Felix Marx</a>
 * @since v7.10.5
 */
public class NextcloudOauthAccountManager implements FileStorageAccountManager {

    private static final class FileStorageAccountImpl implements FileStorageAccount, ServiceAware {

        /**
         * serialVersionUID
         */
        private static final long serialVersionUID = 2796259865206492139L;

        private final FileStorageService  storageService;
        private final Map<String, Object> config;
        private final String              displayName;

        /**
         * Initializes a new {@link NextcloudOauthAccountManager.FileStorageAccountImpl}.
         * 
         * @param leanConfigService
         * @param urlProvider
         * @throws OXException
         * 
         */
        @SuppressWarnings({
            "unchecked",
            "rawtypes" })
        protected FileStorageAccountImpl(
            final FileStorageServiceRegistry fileStorageServiceRegistry,
                final String oauth,
                final Session session,
                LeanConfigurationService leanConfigService,
                NextcloudLoginAndWebdavUrlProvider urlProvider) throws OXException {
            super();
            this.storageService = fileStorageServiceRegistry.getFileStorageService(SERVICE_ID);
            this.config = (Map<String, Object>) new ImmutableMap.Builder().put("authScheme", "Bearer").put("oauth", oauth).put("url", urlProvider.getUrl(session, oauth)).build();
            this.displayName = getDisplayName(leanConfigService, session);
        }

        protected String getDisplayName(LeanConfigurationService leanConfigService, Session session) {
            return leanConfigService.getProperty(session.getUserId(), session.getContextId(), DefaultProperty.valueOf("com.openexchange.file.storage.nextcloud.oauth.displayName", "Nextcloud"));
        }

        @Override
        public Map<String, Object> getConfiguration() {
            return config;
        }

        @Override
        public String getDisplayName() {
            return displayName;
        }

        @Override
        public FileStorageService getFileStorageService() {
            return storageService;
        }

        @Override
        public String getId() {
            return NextcloudOauthAccountManager.ACCOUNT_ID;
        }

        @Override
        public String getServiceId() {
            return SERVICE_ID;
        }

        @SuppressWarnings("unused")
        public JSONObject getMetadata() {
            return new JSONObject();
        }
    }

    public static final String SERVICE_ID = "nextcloud";
    public static final String ACCOUNT_ID = "nextcloud_oauth";

    private final FileStorageServiceRegistry fileStorageServiceRegistry;
    private final LeanConfigurationService   leanConfigService;
    private final CapabilityService          capabilityService;
    private final NextcloudLoginAndWebdavUrlProvider urlProvider;

    public NextcloudOauthAccountManager(
        FileStorageServiceRegistry fileStorageServiceRegistry,
            LeanConfigurationService leanConfigService,
            CapabilityService capabilityService,
            NextcloudLoginAndWebdavUrlProvider urlProvider,
            FileStorageAccountManager delegate) {
        super();
        this.fileStorageServiceRegistry = fileStorageServiceRegistry;
        this.leanConfigService = leanConfigService;
        this.capabilityService = capabilityService;
        this.urlProvider = urlProvider;
        this.delegate = delegate;
    }

    protected FileStorageAccount getAccount0(final String id, final Session session) throws OXException {
        Object oauthAccessToken = getAccessToken(session);
        if (null == oauthAccessToken) {
            throw FileStorageExceptionCodes.ACCOUNT_NOT_FOUND.create(id, ACCOUNT_ID);
        }
        if (!(oauthAccessToken instanceof String)) {
            throw FileStorageExceptionCodes.ACCOUNT_NOT_FOUND.create(id, ACCOUNT_ID);
        }

        String oauthAccessTokenString = (String) oauthAccessToken;
        return new FileStorageAccountImpl(fileStorageServiceRegistry, oauthAccessTokenString, session, leanConfigService, urlProvider);
    }

    protected Object getAccessToken(final Session session) {
        return session.getParameter(Session.PARAM_OAUTH_ACCESS_TOKEN);
    }

    private final FileStorageAccountManager delegate;

    @Override
    public String addAccount(FileStorageAccount account, Session session) throws OXException {
        return delegate.addAccount(account, session);
    }

    @Override
    public void updateAccount(FileStorageAccount account, Session session) throws OXException {
        if (NextcloudOauthAccountManager.ACCOUNT_ID.equals(account.getId())) {
            return;
        }
        delegate.updateAccount(account, session);
    }

    @Override
    public void deleteAccount(FileStorageAccount account, Session session) throws OXException {
        if (NextcloudOauthAccountManager.ACCOUNT_ID.equals(account.getId())) {
            return;
        }
        delegate.deleteAccount(account, session);
    }

    @Override
    public List<FileStorageAccount> getAccounts(Session session) throws OXException {
        List<FileStorageAccount> accounts = delegate.getAccounts(session);
        if (checkCapability(session) && null != getAccessToken(session)) {
            ArrayList<FileStorageAccount> newAccounts = new ArrayList<>(accounts);
            newAccounts.add(getAccount0(SERVICE_ID, session));
            return newAccounts;
        }
        return accounts;
    }

    @Override
    public FileStorageAccount getAccount(String id, Session session) throws OXException {
        if (NextcloudOauthAccountManager.ACCOUNT_ID.equals(id)) {
            return getAccount0(id, session);
        } else {
            return delegate.getAccount(id, session);
        }
    }

    @Override
    public void cleanUp(String secret, Session session) throws OXException {
        delegate.cleanUp(secret, session);
    }

    @Override
    public void removeUnrecoverableItems(String secret, Session session) throws OXException {
        delegate.removeUnrecoverableItems(secret, session);
    }

    @Override
    public void migrateToNewSecret(String oldSecret, String newSecret, Session session) throws OXException {
        delegate.migrateToNewSecret(oldSecret, newSecret, session);
    }

    @Override
    public boolean hasEncryptedItems(Session session) throws OXException {
        return delegate.hasEncryptedItems(session);
    }

    protected boolean checkCapability(final Session session) throws OXException {
        return capabilityService.getCapabilities(session).contains("filestorage_nextcloud_oauth");
    }
}
