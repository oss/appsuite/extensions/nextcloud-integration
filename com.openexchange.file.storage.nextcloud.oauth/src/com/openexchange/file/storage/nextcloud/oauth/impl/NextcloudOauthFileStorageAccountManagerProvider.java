/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.file.storage.nextcloud.oauth.impl;

import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.FileStorageAccountManager;
import com.openexchange.file.storage.FileStorageAccountManagerProvider;
import com.openexchange.file.storage.registry.FileStorageServiceRegistry;
import com.openexchange.file.storage.nextcloud.oauth.NextcloudLoginAndWebdavUrlProvider;
import com.openexchange.file.storage.nextcloud.oauth.osgi.NextcloudOauthFileStorageAccountManagerTracker;
import com.openexchange.session.Session;

/**
 * 
 * {@link NextcloudOauthFileStorageAccountManagerProvider}
 * 
 * @author <a href="mailto:felix.marx@open-xchange.com">Felix Marx</a>
 * @since v7.10.5
 */
public final class NextcloudOauthFileStorageAccountManagerProvider implements FileStorageAccountManagerProvider {

    private final FileStorageServiceRegistry                     fileStoreReg;
    private final LeanConfigurationService                       leanConfig;
    private final CapabilityService                              capService;
    private final NextcloudOauthFileStorageAccountManagerTracker tracker;
    private final NextcloudLoginAndWebdavUrlProvider                     urlProvider;

    public NextcloudOauthFileStorageAccountManagerProvider(
        NextcloudOauthFileStorageAccountManagerTracker tracker,
            FileStorageServiceRegistry fileStoreReg,
            LeanConfigurationService leanConfig,
            CapabilityService CapService,
            NextcloudLoginAndWebdavUrlProvider urlProvider) {
        super();
        this.tracker = tracker;
        this.fileStoreReg = fileStoreReg;
        this.leanConfig = leanConfig;
        this.capService = CapService;
        this.urlProvider = urlProvider;
    }

    @Override
    public boolean supports(String serviceId) {
        return NextcloudOauthAccountManager.SERVICE_ID.equals(serviceId);
    }

    @Override
    public int getRanking() {
        return 20;
    }

    @Override
    public FileStorageAccountManager getAccountManagerFor(String serviceId) throws OXException {
        return new NextcloudOauthAccountManager(fileStoreReg, leanConfig, capService, urlProvider, tracker.getAccountManagerFor(serviceId, this));
    }

    @Override
    public FileStorageAccountManager getAccountManager(String accountId, Session session) throws OXException {
        return new NextcloudOauthAccountManager(fileStoreReg, leanConfig, capService, urlProvider, tracker.getAccountManager(accountId, session, this));
    }
}
