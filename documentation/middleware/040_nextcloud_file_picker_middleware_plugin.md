# File Picker Middleware Plugin for Nextcloud

|||
| --------------------------------- | -------------------------------------------------------------------------------- |
| Story for original implementation | [NCLD-2](https://jira.open-xchange.com/browse/NCLD-2), [NCLD-9](https://jira.open-xchange.com/browse/NCLD-9), [NCLD-8](https://jira.open-xchange.com/browse/NCLD-8)
| Code repository                   | [extensions/nextcloud-integration](https://gitlab.open-xchange.com/extensions/nextcloud-integration)
| Bundle Identifier                 | `com.openexchange.nextcloud.access.token.provider`, `com.openexchange.nextcloud.filepicker`
| Package(s)                        | `open-xchange-nextcloud-filepicker`
| Required capabilities             | `com.openexchange.capability.filestorage_nextcloud_oauth=true`
| Available since                   | 7.10.5-rev1
| Maintainers                       | Carsten Hoeger


The session based Nextcloud file storage allows to use `access_tokens` bound to the session to use attachments from an internal Nextcloud account that is connected via the `open-xchange-nextcloud-oauth-session` storage.

## HTTP API to Create Nextcloud Share Links

### Parameter

| Parameter    | Description                          |
| ------------ | ------------------------------------ |
| `server`     | Hostname or IP of the server.
| `sessionId`  | The session id of the user.

Please use the URL prefix below for following actions:

```
PUT http://{server}/appsuite/api/nextcloud/filepicker?session={sessionId}&action=
```

| Action URL Suffix | Example Body                | Description
| ----------------- | --------------------------- | -----------
| `sharelink`       | `{"path":"/example.png"}`   | The share link path of e.g. `/example.png`.

Parameters of the sharelink Request body, reflects the documentation at [ocs-share-api, create a share](https://docs.nextcloud.com/server/latest/developer_manual/client_apis/OCS/ocs-share-api.html#create-a-new-share).

| Parameter         | Optional   | Description
| ----------------- | ---------- | -----------
| `path`            | `false`    | The share link path of e.g. `/example.png`.
| `shareType`       | `true`     | The share type, defaults to `3` if not set.
| `shareWith`       | `true`     | Target of the share e.g. `test@example.com`.
| `publicUpload`    | `true`     | If public upload should be allowed `true` or `false`.
| `password`        | `true`     | Password for the share e.g. `16%!%374123`, must be secure if set, otherwise share creation fails.
| `permissions`     | `true`     | The share permissions e.g. `3` for read and update.
| `expireDate`      | `true`     | The share expire Date e.g. `2021-09-22`.

**Example `sharelink` Response**

```
{
  "data": {
    "url": "https://sps-nc.duckdns.org/index.php/s/5Q4Sq3ZDrZYTHqZ"
  }
}
```

### Error Codes

| Error  Code    | Description                        |
| -------------- | ----------------------------------
| `NC_FILE-0001` | An error occurred: {details}
| `NC_FILE-0002` | Access token is missing: {details}
| `NC_FILE-0003` | The host is not configured.
| `NC_FILE-0004` | Failed to authenticate against the nextcloud system: {details}
| `NC_FILE-0005` | The created share did not return an url: {details}
| `NC_FILE-0006` | Failed to create a share, error: {details}
| `NC_FILE-0007` | Failed to create a share, error: {details}

`NC_FILE-0007` is special as it is logged as `USER_INPUT`.

## HTTP API to Get AccessToken and User specific webdav Url

### Parameter

| Parameter    | Description                          |
| ------------ | ------------------------------------ |
| `server`     | Hostname or IP of the server.
| `sessionId`  | The session id of the user.

Please use the URL prefix below for following actions:

```
GET http://{server}/appsuite/api/nextcloud/accesstoken?session={sessionId}&action=
```

| Action URL Suffix |  Description
| ----------------- | -----------
| `GET`             | Returns the accessToken and url of the User if present.

**Example `GET` Response**

```
{
  "data": {
    "accessToken": "eyJhbGciOiJSUzI1NiIsImtpZCI6Imtvbm5l....oX38QhflJnLQotZs2Wo",
    "login": "test"
  }
}
```

Note that the accessToken key/value can be omitted in setting:

```
com.openexchange.nextcloud.filepicker.includeAccessToken=false
```

### Error Codes

No special error codes
