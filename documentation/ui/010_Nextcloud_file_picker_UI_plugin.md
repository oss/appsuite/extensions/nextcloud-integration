# File Picker UI Plugin for Nextcloud

|||
| --------------------------------- | -------------------------------------------------------------------------------- |
| Story for original implementation | [NCLD-3](https://jira.open-xchange.com/browse/NCLD-3)
| Code repository                   | [extensions/nextcloud-integration](https://gitlab.open-xchange.com/extensions/nextcloud-integration)
| Package(s)                        | <code>open-xchange-appsuite-nextcloud-filepicker, open-xchange-appsuite-nextcloud-filepicker-static</code>
| Required capabilities             | <code>com.openexchange.capability.filestorage_nextcloud_oauth</code>
| Available since                   | 7.10.5
| Maintainers                       | Viktor Pracht

## Overview

This is the UI part of the integration of the [Nextcloud file picker](https://github.com/eneiluj/nextcloud-webdav-filepicker) into OX App Suite.

![File Picker Dialog](images/dialog.png "File Picker Dialog")

## Installation

The UI plugin is available in the packages `open-xchange-appsuite-nextcloud-filepicker` and `open-xchange-appsuite-nextcloud-filepicker-static`. As usual with UI packages, in case of dedicated web servers, the latter package is installed on the web servers, and the former on middleware nodes.

## Configuration

For a single-node installation where Nextcloud and OX App Suite run on the same domain, the integration should work out of the box. By default, the integration assumes that Nextcloud is available unter `/nextcloud/` on the same domain. If that is not the case, the exact URL of Nextcloud can be configured in

`/opt/open-xchange/etc/settings/nextcloud-filepicker.properties`:

```
# Custom URL for the Nextcloud server.
# Defaults to /nextcloud/ on the same domain as OX App Suite.
#
# io.ox.nextcloud//server=https://server.tld/nextcloud/

# Custom URL for the Nextcloud Files app.
# Defaults to apps/files/ relative to the value of io.ox.nextcloud//server.
#
# io.ox.nextcloud//appUrl=https://server.tld/nextcloud/apps/files/
```

The first option is mainly used to find the WebDAV API endpoint. In addition, the location of the Nextcloud Files app is automatically derived from it by appending `apps/files/`. If this does not work, the exact location can be specified explicitly using the second option.
