/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.nextcloud.access.token.provider.osgi;

import com.google.common.collect.ImmutableMap;
import com.openexchange.ajax.requesthandler.osgiservice.AJAXModuleActivator;
import com.openexchange.config.lean.LeanConfigurationService;
import com.openexchange.file.storage.nextcloud.oauth.NextcloudLoginAndWebdavUrlProvider;
import com.openexchange.nextcloud.access.token.provider.impl.GetTokenAction;

public class AccessTokenProviderActivator extends AJAXModuleActivator {

    private static transient final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AccessTokenProviderActivator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] {
            NextcloudLoginAndWebdavUrlProvider.class,
            LeanConfigurationService.class
        };
    }

    @Override
    protected boolean stopOnServiceUnavailability() {
        return true;
    }

    @Override
    protected void startBundle() throws Exception {
        LOG.info("starting bundle: com.openexchange.nextcloud.access.token.provider");
        registerModuleWithoutMultipleAccess(
            ImmutableMap.of(
                "GET",
                new GetTokenAction(
                    getServiceSafe(NextcloudLoginAndWebdavUrlProvider.class),
                    getServiceSafe(LeanConfigurationService.class)))::get,
            "nextcloud/accesstoken");
    }

}
