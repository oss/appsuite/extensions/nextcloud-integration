# Nextcloud Integration for OX App Suite

This repository contains the integration of the [Nextcloud WebDav file picker](https://github.com/julien-nc/nextcloud-webdav-filepicker) with OX App Suite. This integration replaces the OX Drive app, so the `infostore` permission (`--access-infostore` on the command line) should be disabled.

For the details on how to configure and use the integration, see the [documentation](documentation/) directoy.
